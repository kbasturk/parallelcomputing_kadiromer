import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/* This is a simple Java program.
   FileName : "HelloWorld.java". */
class Main
{
    private static volatile int[] prices;

    public static void main(String args[])
    {
        getPrices();
        int n = prices.length;
        System.out.println(n);

        Instant start = Instant.now();
        System.out.print("Given Array is\n");
        GFG.printArray(prices, n);
        Instant end = Instant.now();

        System.out.println("Time for executing the unsorted array: " + Duration.between(start, end).toNanos());

        Arrays.parallelSort(prices);

        Instant start2 = Instant.now();
        System.out.print("\nAfter Sorting Array is\n");
        GFG.printArray(prices, n);
        Instant end2 = Instant.now();

        System.out.println("Time for executing the sorted array: " + Duration.between(start2, end2).toNanos());

        long difference = Duration.between(start, end).toNanos() - Duration.between(start2, end2).toNanos();

        System.out.println("\n*****************************\nDifference of unsorted and sorted array in time = " + difference + "\n*****************************");

       }

    private static synchronized void getPrices(){
        try{
            File dataset = new File("src/CSV/Schools.csv");
            CSVReader reader = new CSVReader(dataset.getCanonicalPath());
            prices = reader.convertArrayList();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}