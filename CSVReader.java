import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;


public class CSVReader {

    private String filename, row;
    private ArrayList<Integer> entries = new ArrayList<>();

    public CSVReader(String filename){
        this.filename = filename;
        this.readFile();
    }

    private void readFile(){
        try{
            int i = 0;
            BufferedReader csvReader = new BufferedReader(new FileReader(this.filename));
            while ((row = csvReader.readLine()) != null){
                String[] entry = row.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                if(i > 0 && i <= 70000){
                    int price = Integer.parseInt(entry[5]);
                    entries.add(price);
//                    System.out.println("i = " + i + "entry: " + entry[5]);
                }
                i++;
            }
            csvReader.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public ArrayList<Integer> getEntries(){
        return this.entries;
    }

    public int[] convertArrayList(){
        ArrayList<Integer> arrayList = this.entries;
        int[] converted = new int[arrayList.size()];
        for(int i=1; i < converted.length; i++){
            converted[i] = arrayList.get(i);
        }
        return converted;
    }

}